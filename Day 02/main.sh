#!/usr/bin/env bash

codeName="main.py"

baseDir=$(dirname "$PWD")

find . -type f -name "$codeName" -print0 | while IFS= read -r -d $'\0' codeFileGiven; do
    identifier=$(basename "$PWD")
    echo -e "\n\e[1;34;40mrunning:\t\e[0m$codeFileGiven"
    pushd "$(dirname "$codeFileGiven")" > /dev/null
    echo -ne "\e[1;34;40moutput:\t\t\e[0m"
    python -m cProfile -o "$identifier.dat" "$codeName"
    cp -r $baseDir/Analysis ./ > /dev/null
    echo -e "\n\e[1;34;40manalysing:\t\e[0m$codeFileGiven"
    echo -e "strip\nsort time\nstats 25" | python -m Analysis.pstats-f10 "$identifier.dat" > "$identifier.stat"
    echo -e "\e[1;34;40moutput:\t\t\e[0m$identifier.stat"
    rm -r Analysis > /dev/null
    popd > /dev/null
done
