#!/usr/bin/env python

inputFile = "../input"

validPasswords = 0

with open(inputFile) as openFile:
    inputData = [[int(line.split()[0].split("-")[0]),int(line.split()[0].split("-")[1]),line.split()[1][0],line.split()[-1]] for line in openFile.readlines()]

for line in inputData:
    if line[0] <= line[3].count(line[2]) and line[3].count(line[2]) <= line[1]:
        validPasswords += 1

print(validPasswords)