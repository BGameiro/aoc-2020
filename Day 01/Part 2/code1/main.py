#!/usr/bin/env python

inputFile = "../input"

intSum = 2020

with open(inputFile) as openFile:
    inputData = [int(line) for line in openFile.readlines()]

for intGivenFirst in inputData:
    intMatchTemp = intSum - intGivenFirst
    for intGivenSecond in inputData:
        intMatch = intMatchTemp - intGivenSecond
        if intMatch in inputData:
            print("{}*{}*{}={}".format(intGivenFirst,intGivenSecond,intMatch,intGivenFirst*intGivenSecond*intMatch))
            exit()