#!/usr/bin/env python

inputFile = "../input"

intSum = 2020

with open(inputFile) as openFile:
    inputData = [int(line) for line in openFile.readlines()]

for intGiven in inputData:
    intMatch = intSum - intGiven
    if intMatch in inputData:
        print("{}*{}={}".format(intGiven,intMatch,intGiven*intMatch))
        exit()
